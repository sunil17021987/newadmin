import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './common/login/login.component';
import { RegisterComponent } from './common/register/register.component';
import { DashboardComponent } from './common/dashboard/dashboard.component';
import { MasterComponent } from './common/master/master.component';
import { NotfoundComponent } from './common/notfound/notfound.component';
import { LogoutComponent } from './common/logout/logout.component';
import { AuthGuardService } from "./common/services/guards/auth-guard.service";

const routes: Routes = [
{path:"",redirectTo:"admindashboard",pathMatch:"full"},
{path:"login",component:LoginComponent},

{path:"register",component:RegisterComponent},

//{path:"master",component:MasterComponent},
{
  path: '',
  component: MasterComponent,
  canActivate: [AuthGuardService] ,
  children:[
    {
      path:'admindashboard',
      loadChildren:()=> import('./common/admindashboard/admindashboard.module').then(m=>m.AdmindashboardModule)
    },
    {
      path: 'library',
      loadChildren: () => import('./common/library/library.module').then(m =>m.LibraryModule)
    },
    {
     path:'user',
     loadChildren:()=>import('./common/user/user.module').then(m=>m.UserModule)
    }
  ]
},
{
  path:'logout',component:LogoutComponent
},
{
  path:'**',component:NotfoundComponent
}
//{path:"dashboard",component:DashboardComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
