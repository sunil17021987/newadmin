import { Component, OnInit } from '@angular/core';
import { Register } from '../../classes/register';
import { RegisterService } from '../../services/register.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Router} from "@angular/router";
//import 'rxjs/add/operator/filter';
@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent implements OnInit {
  submitted = false;
  users: Register;
  orderid: number;
  regFormEdit: FormGroup;
  register: Register;
  constructor(private registerService: RegisterService,private route: ActivatedRoute,private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.orderid = parseInt(params.get("id"));
     // console.log(this.orderid);
     this.registerService.getUser(this.orderid).subscribe(data=>{
      this.users= data['record'];
      console.log(this.users);
      this.regFormEdit.setValue({
        id:this.users.id,
      firstName:this.users.firstName,
      email:this.users.email,
      password:this.users.password,
      confirmPassword:this.users.confirmPassword,
      address:this.users.address,
      skills:this.users.skills,
      acceptTerms:this.users.acceptTerms
      });


   });
   //********************************************************************** */
     this.regFormEdit = this.formBuilder.group({
      id: ['',Validators.required],
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:['', Validators.required],
      address: ['', Validators.required],
      skills: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
  });



     //****************************************** */
    });

  }


  get f() { return this.regFormEdit.controls; }
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.regFormEdit.invalid) {
        return;
    }

    // display form values on success
   alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.regFormEdit.value, null, 4));
   //this.users: Register;
   this.register=this.regFormEdit.value;
this.registerService.updateData(this.register).subscribe(data=>{
//console.log(data);
this.router.navigate(['/user/userslist']);
});


}

}
