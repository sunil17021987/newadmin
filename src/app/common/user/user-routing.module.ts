import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdduserComponent } from './adduser/adduser.component';
import { UserslistComponent } from './userslist/userslist.component';
import { ShowuserComponent } from './showuser/showuser.component';


const routes: Routes = [
  {
    path: '',
      children: [
        {
          path: '',
          component: AdduserComponent
        },
        {
          path:'showuser/:id',
          component: ShowuserComponent

        },
        {
          path: 'userslist',
          component: UserslistComponent
        },
      ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
