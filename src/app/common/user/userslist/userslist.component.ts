import { Component, OnInit } from '@angular/core';
import { Register } from '../../classes/register';
import { RegisterService } from '../../services/register.service';

@Component({
  selector: 'app-userslist',
  templateUrl: './userslist.component.html',
  styleUrls: ['./userslist.component.css']
})
export class UserslistComponent implements OnInit {
users: Register[];
  constructor(private registerService: RegisterService) { }

  ngOnInit() {
this.registerService.getUsers().subscribe(data=>{
   this.users= data['record'];
})

  }
  delete(user:Register) {
   
    //Delete item in Student data
    this.registerService.deleteItem(user.id).subscribe(data => {
    //  console.log(data);
      this.users= data['record'];
    });
  }
}
