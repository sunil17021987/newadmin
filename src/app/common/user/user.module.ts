import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { UserRoutingModule } from './user-routing.module';
import { AdduserComponent } from './adduser/adduser.component';
import { UserslistComponent } from './userslist/userslist.component';
import { ShowuserComponent } from './showuser/showuser.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AdduserComponent, UserslistComponent, ShowuserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class UserModule { }
