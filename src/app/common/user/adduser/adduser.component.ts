import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MustMatch } from '../../validators/must-match.validator';
import { RegisterService } from '../../services/register.service';
import { Register } from '../../classes/register';
import { Router } from '@angular/router';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  regForm: FormGroup;
  submitted = false;
 register: Register;
  constructor(private formBuilder: FormBuilder,private services: RegisterService, private router: Router) { }

  ngOnInit() {
    this.regForm = this.formBuilder.group({
      id: [],
      firstName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:['', Validators.required],
      address: ['', Validators.required],
      skills: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue]
  },
  {
    validator: MustMatch('password', 'confirmPassword')
}
  );

  }



  get f() { return this.regForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.regForm.invalid) {
          return;
      }

      // display form values on success
     alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.regForm.value, null, 4));
     this.register=this.regForm.value;
this.services.createData(this.register).subscribe(data=>{
console.log(data);
this.router.navigate(['/user/userslist']);
});


  }

}
