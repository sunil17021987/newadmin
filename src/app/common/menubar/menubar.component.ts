import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menubar',
  templateUrl: './menubar.component.html',
  styleUrls: ['./menubar.component.css']
})
export class MenubarComponent implements OnInit {
  userdata:any;
  email:any;
  constructor() { }

  ngOnInit() {
    this.userdata = JSON.parse(localStorage.getItem('currentUser'));
    this.email = this.userdata.email;
   // console.log(this.email);
  }

}
