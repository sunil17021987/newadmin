import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { User } from '../classes/user';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  userForm: FormGroup;
  submitted = false;
 user: User;
 logindata:any='';
  constructor(private router:Router,private formBuilder: FormBuilder,private services: UserService) { }


  ngOnInit() {

    this.userForm = this.formBuilder.group({
      id: [],
      username: ['',Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:[],
   
  });

  }//ng on init end
  get f() { return this.userForm.controls; }

  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.userForm.invalid) {
        return;
    }
    // display form values on success
  // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.logForm.value, null, 4));
   this.user=this.userForm.value;
this.services.postbooks(this.user).subscribe(data=>{
  var obj:any = data;
  if(obj.message==="success"){
    alert('Sucess');
  this.router.navigate(['/admindashboard']);
}
else{
  alert('Error');
}
});
}//on submit end

}//main end
