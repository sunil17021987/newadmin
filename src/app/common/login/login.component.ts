import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegisterService } from '../services/register.service';
import { Register } from '../classes/register';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  logForm: FormGroup;
  submitted = false;
 register: Register;
 logindata:any='';
  constructor(private router:Router,private formBuilder: FormBuilder,private services: UserService) { }

  ngOnInit() {
    this.logForm = this.formBuilder.group({
      id: [],
      firstName: [],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      confirmPassword:[],
      address: [],
      skills: [],
      acceptTerms: []
  });
  }
  get f() { return this.logForm.controls; }

  onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.logForm.invalid) {
          return;
      }
      // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.logForm.value, null, 4));
     this.register=this.logForm.value;
     this.services.userlogin(this.register).subscribe(data=>{
      var response:any = data;
    //this.services.token=response.token;
    alert(JSON.stringify(response.token));
    
    if(response.status){
    localStorage.setItem('currentUser', JSON.stringify(response.records));
    localStorage.setItem("token",response.token);
    this.router.navigate(['/admindashboard']);
    }else{
      alert('User Not Valid please Try Again...');
      this.router.navigate(['/login']);
    }
    });


  }

  login(){
    this.router.navigate(['/admindashboard']);
  }

  regiaterPage(){
    this.router.navigate(['register']);
  }
}
