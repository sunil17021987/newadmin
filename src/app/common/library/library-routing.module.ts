import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IssuebookComponent } from './issuebook/issuebook.component';
import { ManagebookComponent } from './managebook/managebook.component';


const routes: Routes = [
  {
    path: '',
      children: [
        {
          path: '',
          component: IssuebookComponent
        },
        {
          path: 'issuebook',
          component: IssuebookComponent
        },
        {
          path:'managebook',
          component:ManagebookComponent
        }
      ]
      }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryRoutingModule { }
