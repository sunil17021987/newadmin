import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryRoutingModule } from './library-routing.module';
import { IssuebookComponent } from './issuebook/issuebook.component';
import { ManagebookComponent } from './managebook/managebook.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FileSelectDirective } from 'ng2-file-upload';
@NgModule({
  declarations: [IssuebookComponent, ManagebookComponent,FileSelectDirective],
  imports: [
    CommonModule,
    LibraryRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class LibraryModule { }
