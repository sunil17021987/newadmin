import { Component, OnInit,ElementRef } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from "../../classes/book";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { variable } from '@angular/compiler/src/output/output_ast';
import { stringify } from 'querystring';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
@Component({
  selector: 'app-issuebook',
  templateUrl: './issuebook.component.html',
  styleUrls: ['./issuebook.component.css']
})
export class IssuebookComponent implements OnInit {
  private books :Book[]=[];
  private book:Book;
formgroup:FormGroup;

fileData: File = null;
previewUrl:any = null;
fileUploadProgress: string = null;
uploadedFilePath: string = null;

  constructor(private bookservice:BookService, private formbuilder:FormBuilder,private el:ElementRef) { }

  ngOnInit() {
   // console.log('LibraryModule load');
   this.getbooks();

this.formgroup= this.formbuilder.group({
  _id:[''],
  id:[''],
  title:['',Validators.required],
  message:['',Validators.required],
  image:['']

});


  }
  getbooks(){
    this.bookservice.getbooks().subscribe(data=>{
      this.books=data.posts;
     console.log(this.books);
    });
  }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
}

preview() {
  // Show preview
  var mimeType = this.fileData.type;
  if (mimeType.match(/image\/*/) == null) {
    return;
  }

  var reader = new FileReader();
  reader.readAsDataURL(this.fileData);
  reader.onload = (_event) => {
    this.previewUrl = reader.result;
  }
}
  onSubmit(){
// this.book=this.formgroup.value;
// alert(JSON.stringify(this.formgroup.value));

alert(this.formgroup.value._id);
if(this.formgroup.value._id==null || this.formgroup.value._id==''){
const formData = new FormData();
formData.append('id', this.formgroup.value.id);
formData.append('title', this.formgroup.value.title);
formData.append('message', this.formgroup.value.message);
formData.append('image', this.fileData);
  alert(this.formgroup.value.id);
this.bookservice.postbooks(formData).subscribe(response=>{
  this.fileData= null;
  this.previewUrl= null;
  this.fileUploadProgress = null;
  this.uploadedFilePath = null;
  this.formgroup.reset();
  //console.log(response);
  this.getbooks();
});
}
else{
//  alert(this.formgroup.value);
//alert(JSON.stringify(this.formgroup.value));
//object File
//
let datapost: Book | FormData;
//var datapost;
if(this.fileData){
  alert('update with image');
  const formData1 = new FormData();
  formData1.append('id', this.formgroup.value.id);
  formData1.append('title', this.formgroup.value.title);
  formData1.append('message', this.formgroup.value.message);
  formData1.append('image', this.fileData);
  datapost=formData1;

}else{
  datapost=this.formgroup.value;
}


this.bookservice.postbooksupdate(this.formgroup.value._id,datapost).subscribe(data=>{
let response:any=  data;
alert(response.message);
  this.formgroup.reset();
  this.getbooks();
})

}

  }//on submit end

  editdata(book:Book){
   // alert(JSON.stringify(book));
    this.book=book;
   // alert(JSON.stringify(this.book));
    this.previewUrl =this.book.image;
    this.formgroup.setValue(this.book);
  }

  delete(book:Book){
    this.book=book;;
    this.bookservice.postbooksdelete(this.book._id).subscribe(data=>{
    let response:any=  data;
    alert(response.message);
     // this.formgroup.reset();
      this.getbooks();
    })
   }



}
