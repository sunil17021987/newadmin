import { Injectable } from '@angular/core';


import { BehaviorSubject, Observable } from 'rxjs';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { User } from '../classes/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public token:String;
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  url = "http://localhost:3000/api/users";
  constructor(private http: HttpClient) { }
  postbooks(data) {
    return this.http.post(this.url+'/signup',data);
  }

  getToken(){
    return this.token=localStorage.getItem("token");
  }

  userlogin(data) {
    return this.http.post(this.url+'/login',data);
  }

}
