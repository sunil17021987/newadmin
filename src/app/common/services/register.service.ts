import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Register } from '../classes/register';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {


   httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  url = "http://localhost/ciapi/index.php/api/register/"; 
  constructor(private http: HttpClient) {
  }

  createData(reg: Register): Observable<Register> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Register>(this.url + 'createRegister',reg, httpOptions);  
  } 
  
  updateData(reg: Register): Observable<Register> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Register>(this.url + 'updateRegister',reg, httpOptions);
  } 

  loginData(reg: Register): Observable<Register> {  
    const httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json'}) };  
    return this.http.post<Register>(this.url + 'createLogin',reg, httpOptions);  
  } 

  getUsers():Observable<Register>{
    return this.http.get<Register>(this.url+'getUsers');
  }
  getUser(id: number):Observable<Register>{
    return this.http.post<Register>(this.url+'getUser',id,this.httpOptions);
  }
   // Delete item by id
   deleteItem(id: any) {
    return this.http
      .post<Register>(this.url + 'studentdelete' , id, this.httpOptions)
      .pipe(
      
      
      )
  }






}
