import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, from, Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Book } from "../classes/book";
@Injectable({
  providedIn: 'root'
})
export class BookService {
  private book :Book[]=[];
 private booksUpdated = new Subject<Book[]>();
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
  url = "http://localhost:3000/api/posts";
  //http://localhost:3000/api/posts
  constructor(private http: HttpClient) { }

getbooksbyobject() {
  return this.http.get<{message:string,posts:Book[]}>(this.url).subscribe(data=>{
    this.book=data.posts;
    this.booksUpdated.next([...this.book]);
  });
}

getBookUpdateListners(){
  return this.booksUpdated.asObservable();
}
getbooks() {
  return this.http.get<{message:string,posts:Book[]}>(this.url);
}
postbooks(data) {
  return this.http.post(this.url,data);
}

postbooksupdate(id:String,data) {
  return this.http.put(this.url+"/"+id,data);
}

postbooksdelete(id:String){
  return this.http.delete(this.url+"/"+id);
}
}
