import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, Route } from '@angular/router';
import { Observable } from 'rxjs';
import { RegisterService } from '../register.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  userdata:any;
  constructor(private regservice: RegisterService, private router: Router) {
  }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.userdata = localStorage.getItem('currentUser');
    if (this.userdata!=null) {
    //  alert('ok');
        return true;
    }

    // navigate to login page
    this.router.navigate(['/login']);
    // you can save redirect url so after authing we can move them back to the page they requested
    return false;
  }
}
