const mongoose = require("mongoose");

const postSchema = mongoose.Schema({
  id:{ type: Number, required: true },
  title: { type: String, required: true },
  message: { type: String, required: true },
  image: { type: String, required: true }
});

module.exports = mongoose.model("Post", postSchema);
